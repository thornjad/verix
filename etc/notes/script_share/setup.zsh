#!/bin/sh

apt update

mkdir ~/.oh-my-zsh/custom/themes

cd ~/etc

git clone git@github.com:Raindeer44/dotfiles.git
cd dotfiles
cp zsh/.zshrc ~/.zshrc
cp tmux/.tmux.conf ~/.tmux.conf

cd ~/src

git clone git@gitlab.com:raindeer/vero.git
cd vero
make install

cd ~/src

git clone git@github.com:rupa/z.git

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
echo "source ${(q-)PWD}/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ${ZDOTDIR:-$HOME}/.zshrc

cd ~/
curl https://raw.githubusercontent.com/amix/vimrc/master/vimrcs/basic.vim > .vimrc

zsh
source .zshrc
chsh -s /bin/zsh
