// Verix kernel main

#![cfg_attr(not(test), no_std)]
#![cfg_attr(not(test), no_main)]
#![cfg_attr(test, allow(unused_imports))]

#![feature(custom_test_frameworks)]
#![test_runner(crate::test_runner)]

use core::panic::PanicInfo;

mod vga_buffer;

#[cfg(not(test))] // only compile when the test flag is not set
#[panic_handler]
fn panic(_info: &PanicInfo) -> ! {
	println!("{}", _info);
	loop {}
}

#[cfg(not(test))]
#[no_mangle]
pub extern "C" fn _start() -> ! {
	println!("Hello World {}", "from our own println macro!");
	loop {}
}

#[cfg(test)]
fn test_runner(tests: &[&dyn Fn()]) {
	println!("Running {} tests", tests.len());
	for test in tests {
		test();
	}
}
